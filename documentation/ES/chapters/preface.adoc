== Prefacio
=== Confidencialidad, Copyright y Responsabilidad

Este es un documento orientado al cliente entre Red Hat ©, Inc. y {customer}. Copyright 2021 Red Hat, Inc. Todos los derechos reservados. Ninguno de los trabajos protegidos por derechos de autor en este documento puede reproducirse o usarse de ninguna manera o por medios gráficos, electrónicos o mecánicos, incluidos los sistemas de fotocopiado, grabación o almacenamiento y recuperación de información, sin el permiso por escrito de Red Hat, excepto cuando sea necesario para compartir esta información según lo previsto con las partes confidenciales mencionadas anteriormente.


=== Introducción
El programa Cloud Success Architecture (CSA) es un programa financiado por CEE que proporciona un compromiso flexible y de alto contacto para ayudar a los clientes con la adopción de las tecnologías de nube híbrida de Red Hat. El servicio está dirigido a clientes estratégicos que han comprado suscripciones a productos Red Hat Cloud: OpenStack, OpenShift Container Platform, Ansible o CloudForms.


=== Sobre este documento
El objetivo de este documento es informar los resultados de la ejecución de la certificación de la plataforma instalada en el entorno Producción.


=== Audiencia
Este documento está orientado al equipo de administradores de sistema, arquitectos y desarrolladores de {customer}.



=== Nomenclatura Checklist
Las siguientes imágenes ayudarán en la revisión de los puntos de control que contempla este documento:

.Nomenclatura Checklist
[cols=2,cols="^1,<5",options=header]
|===
<|Imagen <|Explicación

|image:yes.png[] | Pasa la revisión, no existen comentarios.
|image:warn.png[] | Existen comentarios sobre los resultados obtenidos
|image:no.png[] | No pasa la revisión, se entregan recomendaciones.

|===

<<<
=== Terminología

.Terms Table
[cols=2,cols="^1,<5",options=header]
|===
<|Sigla <|Definición

|OCP | Openshift Container Platform
|OCS | Openshift Container Storage
|CSA | Cloud Success Architect
|CEE | Customer Experience & Engagement
|API | Application Programming Interface
|DNS | Domain Name System
|DHCP| Dynamic Host Configuration Protocol
|FQDN| Fully Qualified Domain Name
|NTP | Network Time Protocol
|OS  | Operating System
|PV  | Persistent Volume
|RHEL| Red Hat Enterprise Linux

|===
