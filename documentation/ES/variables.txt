:subject: Revisión de Ambiente Openshift 4.x
:subtitle: Preparado para
:name:  Robson Watt
:email:  robson@redhat.com
:date: date
:description:  OCP Revisión de entorno
:doctype: book
:confidentiality: Confidencial
:customer:  Customer
:toclevels: 6
:nolang:
:numbered:
:chapter-label:
:lang: es
:icons: font
:pdf-page-size: A4
:pdf-style: redhat
:pdf-stylesdir: .
:revnumber: 3.0
:imagesdir: images

:ambiente: Producción

:toc-title: Índice
:appendix-caption:
:caution-caption: Warning
:example-caption: Example
:figure-caption: Imagen
:table-caption: Tabla
:version-label: Versiones:
